﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32; // Registryを使用するのに必要

namespace CSharpRegistry
{
    class RegistryUtils
    {
        //! サブキー
        private static readonly string SubKey = @"Software\Newral\CSharpRegistry";

        /**
         * @brief コンストラクタ
         *        インスタンス化させない。
         */
        private RegistryUtils() { }

        /**
         * @brief レジストリから値を取得します。
         *
         * @param [in] subKey サブキー
         * @param [in] name 名前
         * @return 値
         */
        public static T ReadValue<T>(string subKey, string name)
        {
            var regKey = Registry.CurrentUser.OpenSubKey(SubKey);
            if (regKey == null)
            {
                return default(T);
            }
            T value = (T)regKey.GetValue(name);
            return value;
        }

        /**
         * @brief レジストリから値を取得します。
         *        サブキーは固定とします。
         *
         * @param [in] name 名前
         * @return 値
         */
        public static T ReadValue<T>(string name)
        {
            return ReadValue<T>(SubKey, name);
        }

        /**
         * @brief レジストリに値を設定します。
         *
         * @param [in] subKey サブキー
         * @param [in] name 名前
         * @param [in] value 値
         */
        public static void WriteValue<T>(string subKey, string name, T value)
        {
            var regKey = Registry.CurrentUser.CreateSubKey(SubKey);
            regKey.SetValue(name, value);
        }

        /**
         * @brief レジストリから値を取得します。
         *        サブキーは固定とします。
         *
         * @param [in] name 名前
         * @param [in] value 値
         */
        public static void WriteValue<T>(string name, T value)
        {
            WriteValue<T>(SubKey, name, value);
        }
    }
}
