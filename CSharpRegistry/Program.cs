﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace CSharpRegistry
{
    class Program
    {
        static void Main(string[] args)
        {
            RegistryUtils.WriteValue<string>("param1", "value1");
            string value = RegistryUtils.ReadValue<string>("param1");
            Debug.WriteLine($"param1={value}");
        }
    }
}
